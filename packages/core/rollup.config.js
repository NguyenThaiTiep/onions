import { defineConfig } from "rollup";
import esbuild from "rollup-plugin-esbuild";
import typescript from "@rollup/plugin-typescript";

export default defineConfig([
  {
    input: "src/index.ts",
    external: (id) => /^[./]/.test(id),
    plugins: [esbuild(), typescript()],
    output: [
      {
        dir: "dist",
        format: "es",
        entryFileNames: `[name].js`,
        preserveModules: true,
        sourcemap: true,
      },
      {
        dir: "dist",
        format: "cjs",
        entryFileNames: `[name].es.js`,
        preserveModules: true,
        sourcemap: true,
      },
      {
        dir: "dist",
        format: "es",
        entryFileNames: `[name].d.ts`,
      },
    ],
  },
]);
