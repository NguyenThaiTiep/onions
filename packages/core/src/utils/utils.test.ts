/* ======color==== */

import { rgbaToHex } from "./color";

describe("color function run exactly", () => {
  test("rgbaToHex run exactly", () => {
    expect(rgbaToHex(0, 51, 255)).toBe("#0033ff");
    expect(rgbaToHex(0, 51, 255, 0.5)).toBe("#0033ff80");
  });
});
