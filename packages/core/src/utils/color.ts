/* ==== COLOR ==== */

export interface RBGAColor {
  green: string;
  red: string;
  blue: string;
  alpha?: number;
}
export const genreateColor = (key: string, value: string) => {};
export const generateAlphaColor = (
  key: string,
  value: string,
  alpha: number
) => {};

const componentToHex = (val: number) => {
  const hex = val.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
};

export const Hex2RgbA = () => {
  return {};
};

export const rgbaToHex = (
  red: number,
  green: number,
  blue: number,
  alpha = 1
) => {
  const alphaHex = componentToHex(Math.round(alpha * 255));
  const valueAlpha = alphaHex == "ff" ? "" : alphaHex;
  return (
    "#" +
    componentToHex(red) +
    componentToHex(green) +
    componentToHex(blue) +
    valueAlpha
  );
};
